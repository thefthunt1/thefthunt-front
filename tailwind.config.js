const colors = require("tailwindcss/colors")
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx,vue}",
    "./node_modules/vue-tailwind-datepicker/**/*.js"
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        red: {
          50: '#FFD6D6',
          100: '#FFB4B4',
          200: '#FF9292',
          300: '#FF7070',
          400: '#951D12',
          500: '#920000',
          600: '#810000',
          700: '#700000',
          800: '#3D0000',
          900: '#1B0000',
          "vtd-primary": colors.sky, // Light mode Datepicker color
          "vtd-secondary": colors.gray, // Dark mode Datepicker color
        },
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
