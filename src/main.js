import { createApp } from 'vue'
import { createPinia } from 'pinia'
import vue3GoogleLogin from 'vue3-google-login'
import gAuthPlugin from 'vue3-google-oauth2';
import App from './App.vue'
import router from './router'
import InfiniteLoading from "v3-infinite-loading";
import "v3-infinite-loading/lib/style.css";

import './index.css'



const app = createApp(App)

app.use(createPinia())
app.use(router)
app.component("infinite-loading", InfiniteLoading);
app.use(vue3GoogleLogin, {
  clientId: '805263091118-8abkebg7q7a90rac1m07mejckss8b5on.apps.googleusercontent.com',
  login_uri: '127.0.0.1:5173/login'
})
// app.use(axios, {
//   baseUrl: 'https://test.com/',
// })

app.mount('#app')
