import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../views/HomePage.vue'
import LoginSignup from '@/views/LoginSignup.vue'
import ItemDetails from '@/views/ItemDetails.vue'
import ProfilePage from '@/views/ProfilePage.vue'
import MyBookmarks from '@/views/MyBookmarks.vue'
import CreateItem from '@/views/CreateItem.vue'
import CategoryItems from '@/views/CategoryItems.vue'
import Success from '@/views/payment/Success.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage,
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: LoginSignup
    },
    {
      path: '/profile',
      name: 'profile',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: ProfilePage
    },
    {
      path: '/item/:id',
      name: 'item',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: ItemDetails
    },
    {
      path: '/category/:id/',
      name: 'categoryItem',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: CategoryItems
    },
    {
      path: '/myBookmarks',
      name: 'myBookmarks',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: MyBookmarks
    },
    {
      path: '/createItem',
      name: 'createItem',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: CreateItem
    },
    {
      path: '/payment/success',
      name: 'paymentSuccess',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: Success
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LoginSignup.vue')
    }
  ]
})

export default router
