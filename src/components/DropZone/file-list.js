import { ref } from 'vue'
import axios from "axios";
import {useWizardStore} from "@/stores/createStepper";
export default function () {
  const files = ref([])

  const {wizardState, setWizardState} = useWizardStore()
  function addFiles(newFiles) {
    let newUploadableFiles = [...newFiles]
      .map((file) => new UploadableFile(file))
      .filter((file) => !fileExists(file.id))
    files.value = files.value.concat(newUploadableFiles)
    wizardState.value[2].images = files.value
  }

  function addUrlFiles(newFiles){
    let newUploadableFiles = [...newFiles]
      .map((file) => new UrlFile(file.image_path, file.id))
      .filter((file) => !fileExists(file.id))
    files.value = files.value.concat(newUploadableFiles)
  }

  function fileExists(otherId) {
    return files.value.some(({ id }) => id === otherId)
  }

  function removeFile(file) {
    if(file.id && !file.file){
      axios.delete(`/items/images/${file.id}`)
        .then(res => {
          const index = files.value.indexOf(file)

          if (index > -1) files.value.splice(index, 1)
          wizardState.value[2].images = files.value
        }).catch(error=>{
        console.log(error)
      })
    } else  {
      const index = files.value.indexOf(file)

      if (index > -1) files.value.splice(index, 1)
      wizardState.value[2].images = files.value
    }

  }

  return { files, addFiles, removeFile, addUrlFiles }
}

class UploadableFile {
  constructor(file) {
    this.file = file
    this.id = `${file.name}-${file.size}-${file.lastModified}-${file.type}`
    this.name= file.name
    this.url = URL.createObjectURL(file)
    this.status = null
  }
}

class UrlFile {
  constructor(url, id) {
    this.id = id
    this.name= url
    this.url = url
    this.status = null
  }
}
