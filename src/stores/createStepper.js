import {defineStore} from "pinia";
import {computed, ref} from "vue";

export const useWizardStore = defineStore('wizard', () => {
  const wizard = ref({
    1: {
      mode: 'lost'
    },
    2: {
      name: '',
      description: '',
      category: '',
      color: '',
      brand: '',
      value: '',
      inscription:'',
      images: [],
      date: ''
    },
    3:{
      postalCode: '', address: '', place: '', country: ''
    }
  })
  const wizardState = computed(() => wizard)


  function setWizardState(step, data) {
    wizard.value[step] = data
  }


  return { setWizardState, wizardState}
})
