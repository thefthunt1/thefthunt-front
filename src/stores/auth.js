import {computed, ref} from "vue";
import {defineStore} from "pinia";

export const useAuthStore = defineStore('auth', () => {
  const data = ref(null)
  const loggedIn = ref(false)
  const user = computed(() => data)
  const isLoggedIn = computed(() => loggedIn)

  function setUserStore(user) {
    data.value = user
  }

  function setLoggedIn(value){
    loggedIn.value = value
  }
  return {  user, setUserStore, isLoggedIn, setLoggedIn }
})
