import {computed, ref} from "vue";
import {defineStore} from "pinia";

export const useSpinnerStore = defineStore('spinner', () => {
  const isLoading = ref(false)
  const loading = computed(() => isLoading)

  function setLoading(value) {
    isLoading.value = value
  }


  return { loading, setLoading }
})
