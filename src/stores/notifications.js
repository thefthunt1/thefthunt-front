import {computed, ref} from "vue";
import {defineStore} from "pinia";

export const useNotificationStore = defineStore('notifications', () => {
  const notification = ref({
    state: false,
    label: '',
    type: 'success',
    text: ''
  })
  const notificationState = computed(() => notification)

  function setNotification(type, label, text) {
    notification.value.state = true
    notification.value.type = type
    notification.value.label = label
    notification.value.text = text
    setTimeout(() => {
      notification.value.state = false
    }, 2000)
  }
  function removeNotification() {
    notification.value.state = false
  }


  return { setNotification, notificationState, removeNotification}
})
