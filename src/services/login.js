import api from "./api";
import {removeUser, setUser} from "@/services/tokenService";
import {useAuthStore} from "@/stores/auth";

export const login =({ email, password }) => {
  const {setUserStore,setLoggedIn} = useAuthStore()
  return api
    .post("/login", {
      email,
      password,
      device: "Admin Panel"
    })
    .then((response) => {
      if (response.data.data.token) {
        setUserStore(response.data.data)
        setLoggedIn(true)
        setUser(response.data.data);
      }
      return response.data;
    });
}


export const checkUser = () => {
  const {setUserStore,setLoggedIn} = useAuthStore()
  return api
    .get("/me")
    .then((response) => {
      if (response.data.data.token) {
        setUserStore(response.data.data)
        setLoggedIn(true)
        setUser(response.data.data);
      }
      return response.data;
    });
}

export const logout = () => {
  removeUser();
}

export const register = ({ username, email, password }) => {
  return api.post("/auth/signup", {
    username,
    email,
    password
  });
}

