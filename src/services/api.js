
import axios from "axios";
import { getLocalAccessToken } from "@/services/tokenService";
import { useSpinnerStore } from "@/stores/loader";
import { useNotificationStore } from "@/stores/notifications";


const instance = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});


instance.interceptors.request.use(
  (config) => {
    const { setLoading, loading } = useSpinnerStore()
    const token = getLocalAccessToken();
    if (!config.noSpinner) {
      setLoading(true)
    }

    if (token) {
      config.headers["Authorization"] = 'Bearer ' + token;  // for Spring Boot back-end
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(function (response) {
  const { setLoading } = useSpinnerStore()
  setLoading(false)
  return response;
}, (error) => {
  const { setLoading } = useSpinnerStore()
  const { setNotification } = useNotificationStore()
  let notificationMessage = error.response?.data?.message || 'Something went wrong';
  if (error.response?.status === 401) {
    if (error.response?.config?.url === '/bookmark') {
      notificationMessage = 'User is not logged. Please signin or register.'
      setNotification('error', 'Error', notificationMessage)
    } else {
      window.location = '/login'
    }
  }

  setLoading(false)
  setNotification('error', 'Error', notificationMessage)
  return Promise.reject(error);
});

// instance.interceptors.response.use(
//   (res) => {ß
//     return res;
//   },
//   async (err) => {
//     const originalConfig = err.config;
//
//     if (originalConfig.url !== "/auth/signin" && err.response) {
//       // Access Token was expired
//       if (err.response.status === 401 && !originalConfig._retry) {
//         originalConfig._retry = true;
//
//         try {
//           const response = await instance.post("/auth/refreshtoken", {
//             refreshToken: getLocalRefreshToken(),
//           });
//
//           const { accessToken } = response.data;
//
//           store.dispatch('auth/refreshToken', accessToken);
//           updateLocalAccessToken(accessToken);
//
//           return instance(originalConfig);
//         } catch (_error) {
//           return Promise.reject(_error);
//         }
//       }
//     }
//
//     return Promise.reject(err);
//   }
// );


export default instance;
