export const formatCurrency = new Intl.NumberFormat('en-NL', {
  style: 'currency',
  currency: 'EUR',
});

export const truncateString = (str, num) => {
  if (str?.length > num) {
    return str.slice(0, num) + "...";
  } else {
    return str;
  }
}
